===================
kabaret.script_view
===================

The kabaret.script_view package is a kabaret GUI extension.
It defines a "Script" view where you can edit and execute python scripts using 'self' as a selected flow Object.

